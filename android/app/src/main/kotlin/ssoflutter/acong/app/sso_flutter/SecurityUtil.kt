package app.connectify.smartinfobot

import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.security.MessageDigest
import javax.crypto.Cipher
import javax.crypto.spec.GCMParameterSpec
import javax.crypto.spec.SecretKeySpec
import javax.inject.Inject
import kotlin.random.Random

class SecurityUtil
@Inject
constructor() {
    private val cipher by lazy {
        Cipher.getInstance("AES/GCM/NoPadding")
    }
    private val charset by lazy {
        charset("UTF-8")

    }

    private val json = Json { encodeDefaults = true }
    fun encryptData(text: String): Pair<String, String> {
        val iv = generateIv()
        val data = Json.encodeToString(text)
        cipher.init(Cipher.ENCRYPT_MODE, generateKey(securityKeyAlias), GCMParameterSpec(128, iv))
        val encryptedData =
            cipher.doFinal(data.toByteArray(charset)).joinToString(bytesToStringSeparator)
        return Pair(encryptedData, iv.joinToString(bytesToStringSeparator))
    }


    fun decryptData(encryptedData: String, iv: String): String {
        val data = encryptedData.split(bytesToStringSeparator).map { it.toByte() }.toByteArray()
        val ivKey = iv.split(bytesToStringSeparator).map { it.toByte() }.toByteArray()
        cipher.init(Cipher.DECRYPT_MODE, generateKey(securityKeyAlias), GCMParameterSpec(128, ivKey))
        return json.decodeFromString(cipher.doFinal(data).toString(charset))
    }

    private fun generateKey(password: String): SecretKeySpec {
        val digest: MessageDigest = MessageDigest.getInstance("SHA-256")
        val bytes = password.toByteArray()
        digest.update(bytes, 0, bytes.size)
        val key = digest.digest()
        val secretKeySpec = SecretKeySpec(key, "AES")
        return secretKeySpec
    }

    private fun generateIv(): ByteArray {
        val iv = ByteArray(12) // 12 bytes for GCM
        Random.nextBytes(iv)
        return iv
    }

    companion object {
        const val securityKeyAlias = "data-store"
        const val bytesToStringSeparator ="|"
    }
}