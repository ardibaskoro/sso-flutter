package ssoflutter.acong.app.sso_flutter
object Constants {
    val BASE_KEY = "app.connectify.panicbutton"
    val SOS ="SOS"
    val IMAGE_VIDEO = "IMAGE,VIDEO"
    val IMAGE = "IMAGE"
    const val APP_NAME = "PB"
    const val TOKEN_PROVIDER_URI = "content://app.aksa.notes.data.contentprovider/token"
    const val TOKEN_KEY = "token"

    const val IV= "iv"
    const val IV_PROVIDER_URI = "content://app.aksa.notes.data.contentprovider/$IV"
}