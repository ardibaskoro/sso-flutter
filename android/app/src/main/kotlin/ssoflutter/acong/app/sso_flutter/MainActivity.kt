package ssoflutter.acong.app.sso_flutter

import android.content.Context
import android.os.Build
import android.webkit.CookieManager
import androidx.annotation.NonNull
import androidx.annotation.RequiresApi
import app.connectify.smartinfobot.SecurityUtil
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel
import org.jboss.aerogear.android.core.Callback

class MainActivity : FlutterActivity() {
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)
        MethodChannel(
            flutterEngine.dartExecutor.binaryMessenger, "CHAT_AI_NATIVE_HELPER"
        ).setMethodCallHandler { call, result ->
            when (call.method) {
                "getToken" -> {
                    try {
                        val getToken = getAccessTokenContentProvider(this@MainActivity)
                        if(getToken.first.isNotBlank()) {
                            val data = mapOf(
                                "token" to getToken.first,
                                "iv" to getToken.second
                            )
                            result.success(data)
                        }else {
                            result.error("GET_TOKEN_FAILED","token is empty","")
                        }
                    } catch (e: Throwable) {

                        result.error("GET_TOKEN_ERROR","token is empty ${e.message}","")
                    }
                }
                "storeToken" -> {
                    val token = call.argument("token") ?: ""
                    val iv = call.argument("iv") ?: ""
                    storeTokenContentProvider(Pair(token,iv),this)
                    CookieManager.getInstance().removeAllCookies(null)
                    result.success(true)
                }
                "decrypt" -> {
                    val token = call.argument("token") ?: ""
                    val iv = call.argument("iv") ?: ""
                    val decrypt = SecurityUtil().decryptData(token,iv)
                    result.success(decrypt)
                }
                "connect" -> {
                    try {
                        KeycloakHelper.connect(this, object : Callback<String?> {
                            override fun onSuccess(data: String?) {
                                val encrypt =SecurityUtil().encryptData(data.orEmpty())
                                CookieManager.getInstance().removeAllCookies(null)
                                storeTokenContentProvider(encrypt,this@MainActivity)
                                val data = mapOf(
                                    "token" to encrypt.first,
                                    "iv" to encrypt.second
                                )
                                result.success(data)

                            }

                            override fun onFailure(e: Exception) {
                                result.error("KEYCLOAK_CONNECT_ERROR", e.message, e)
                            }

                        })

//                        result.error(
//                            "KEYCLOAK_CONNECT_ERROR",
//                            "Activity Not Found",
//                            "Activity Not Found",
//                        )
                    } catch (e: Throwable) {
                        result.error("KEYCLOAK_CONNECT_ERROR", e.message, e)
                    }
                }
            }
        }
    }

    private fun connect(result: MethodChannel.Result) {
        KeycloakHelper.connect(this, object : Callback<String?> {
            override fun onSuccess(data: String?) {
                result.success(data)
            }

            override fun onFailure(e: Exception) {
                result.error("KEYCLOAK_CONNECT_ERROR", e.message, e)
            }

        })
    }

}
