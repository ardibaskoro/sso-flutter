package ssoflutter.acong.app.sso_flutter


import android.app.Activity
import android.util.Log
import org.jboss.aerogear.android.authorization.AuthorizationManager
import org.jboss.aerogear.android.authorization.oauth2.OAuth2AuthorizationConfiguration
import org.jboss.aerogear.android.core.Callback
import java.net.URL
import java.util.Arrays


object KeycloakHelper {
    private const val AUTH_SERVER_URL = "https://sso-portal.kejaksaan.go.id"
    private const val AUTH_ENDPOINT = "/realms/master/protocol/openid-connect/auth"
    private const val ACCESS_TOKEN_ENDPOINT = "/realms/master/protocol/openid-connect/token"
    private const val REFRESH_TOKEN_ENDPOINT = "/realms/master/protocol/openid-connect/token"
    private const val AUTH_CLIENT_ID = "PTT-MOBILE"
    private const val AUTH_REDIRECT_URL = "oauth://oauth2callback"
    private const val MODULE_NAME = "KeyCloakAuthz"
    val SCOPES: List<String> = Arrays.asList("openid")
    private val TAG = KeycloakHelper::class.java.simpleName

    init {
        try {
            AuthorizationManager.config(
                MODULE_NAME,
                OAuth2AuthorizationConfiguration::class.java
            )
                .setBaseURL(URL(AUTH_SERVER_URL))
                .setAuthzEndpoint(AUTH_ENDPOINT)
                .setAccessTokenEndpoint(ACCESS_TOKEN_ENDPOINT)
                .setRefreshEndpoint(REFRESH_TOKEN_ENDPOINT)
                .setClientId(AUTH_CLIENT_ID)
                .setAccountId(AUTH_CLIENT_ID)
                .setRedirectURL(AUTH_REDIRECT_URL)
                .setScopes(SCOPES)
                .asModule()
        } catch (e: Exception) {
            throw RuntimeException(e)
        }
    }

    fun connect(activity: Activity?, callback: Callback<String?>) {
        Log.i(TAG, "Run Connect ")
        val authzModule = AuthorizationManager.getModule(MODULE_NAME)
        authzModule.requestAccess(activity, object : Callback<String> {
            override fun onSuccess(data: String) {
                Log.i(TAG, "loging data $data")
                callback.onSuccess(data)
            }

            override fun onFailure(e: Exception) {
                Log.e(TAG, e.message, e)
                callback.onFailure(e)
            }
        })
    }

    fun deleteAccount() {
        AuthorizationManager.getModule(MODULE_NAME).deleteAccount()
    }

    val isConnected: Boolean
        get() {
            Log.i(TAG, "check is connected!")
            return AuthorizationManager.getModule(MODULE_NAME).isAuthorized
        }
}
