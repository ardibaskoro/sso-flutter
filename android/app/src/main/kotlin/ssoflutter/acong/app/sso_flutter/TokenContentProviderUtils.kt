package ssoflutter.acong.app.sso_flutter

import android.content.ContentValues
import android.content.Context
import android.net.Uri
import ssoflutter.acong.app.sso_flutter.Constants.IV
import ssoflutter.acong.app.sso_flutter.Constants.IV_PROVIDER_URI
import ssoflutter.acong.app.sso_flutter.Constants.TOKEN_KEY
import ssoflutter.acong.app.sso_flutter.Constants.TOKEN_PROVIDER_URI

public fun storeTokenContentProvider(data: Pair<String,String>, context: Context?) {
    val uri = Uri.parse(TOKEN_PROVIDER_URI)
    val values = ContentValues().apply { put(TOKEN_KEY, data.first) }
    val uriIV = Uri.parse(IV_PROVIDER_URI)
    val valuesIV = ContentValues().apply { put(IV, data.second) }
    context?.contentResolver?.insert(uri, values)
    context?.contentResolver?.insert(uriIV, valuesIV)
}


public fun getAccessTokenContentProvider(context: Context?): Pair<String,String> {
    var accessToken  = ""
    val uri = Uri.parse(TOKEN_PROVIDER_URI)
    val projection = arrayOf(TOKEN_KEY)

    val cursor = context?.contentResolver?.query(uri, projection, null, null, null)

    cursor?.use {
        if (it.moveToFirst()) {
            accessToken = it.getString(it.getColumnIndexOrThrow(TOKEN_KEY))
        }
    }

    var iv = ""
    val uriIv = Uri.parse(IV_PROVIDER_URI)
    val projectionIv = arrayOf(IV)

    val cursorIV = context?.contentResolver?.query(uriIv, projectionIv, null, null, null)

    cursorIV?.use {
        if (it.moveToFirst()) {
            iv = it.getString(it.getColumnIndexOrThrow(IV))
        }
    }

    return Pair(accessToken, iv)
}
