# sso_flutter

A new SSO Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

### How to Create

1. Salin kode berikut pada file gradle yang terdapat pada android/app/build.gradle, salin tepat
   didalam tag kode dependencies

```groovy
dependencies {
    ...
    implementation("org.jboss.aerogear:aerogear-android-authz:4.0.0")
}
```

2. Salin kode berikut pada AndroidManifest.xml yang terdapat pada folder android, salin pada bagian
   dalam kode <activity>
   atau salin tepat di atas tutup tag activity </activity>

```xml

<activity...>
    <intent-filter>
        <action android:name="android.intent.action.VIEW" />
        <category android:name="android.intent.category.DEFAULT" />
        <category android:name="android.intent.category.BROWSABLE" />
        <data android:host="oauth2callback" android:scheme="oauth" />
    </intent-filter>
</activity...>
```

3. Masih di file yang sama, salin kode berikut tepat dibawah penutup tag activity </activity>

```xml

<service android:name="org.jboss.aerogear.android.authorization.oauth2.OAuth2AuthzService" />
```

4. Salin KeycloakHelper.kt dan sesuaikan dengan package id yang ada
5. Tambahkan kode berikut pada MainActivity

```kotlin
MethodChannel(
    flutterEngine.dartExecutor.binaryMessenger, "KEYCLOAK"
).setMethodCallHandler { call, result ->
    when (call.method) {
        "connect" -> {
            try {
                if (!KeycloakHelper.isConnected) {
                    KeycloakHelper.connect(this, object : Callback<String?> {
                        override fun onSuccess(data: String?) {
                            result.success(data)
                        }

                        override fun onFailure(e: Exception) {
                            result.error("KEYCLOAK_CONNECT_ERROR", e.message, e)
                        }

                    })
                }
            } catch (e: Throwable) {
                result.error("KEYCLOAK_CONNECT_ERROR", e.message, e)
            }
        }
    }
}
```

kode ini digunakan untuk menghubungkan KeycloakHelper agar dapat digunakan oleh aplikasi flutter.

**Untuk memanggil kode yang telah dibuat pada aplikas flutter, bisa lakukan langkah-langkah berikut,**

*Gunakan methodchannel untuk memanggil function pada MainActivity*
```dart
// import package flutter service untuk menggunakan fungsi MethodChannel
import 'package:flutter/services.dart';

// panggil method channel pada class yang anda buat, dan pastikan nama nya sama dengan yang ada pada MainActivity.
final platform = MethodChannel('KEYCLOAK');

// Setelah itu gunakan fungsi invokeMethod untuk memanggil fungsi "connect" pada AndroidManifest yang telah dibuat
platform.invokeMethod('connect');
```
*Anda bisa mendapatkan result dengan cara seperti contoh berikut*
```dart
// misalnya adalah fungsi connect
// fungsi ini akan mengembalikan nilai dynamic
Future<dynamic> connect() async {
  // inisiasi methodchannelnya 
  final platform = MethodChannel('KEYCLOAK');
  
  // kemudian panggil fungsi invokeMethodnya
  final result = platform.invokeMethod('connect');
  
  return result;
}
```
fungsi diatas akan mengasilkan keluaran berupa hasil dari fungsi connect yang dijalankan. 
