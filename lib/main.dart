import 'package:flutter/material.dart';
import 'helpers/keycloak_helper.dart';
import 'helpers/content_provider_helper.dart';
import 'package:appcheck/appcheck.dart';

import 'dart:developer';
// import 'package:keycloak_flutter/keycloak_flutter.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'SSO Login'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool isAppInstalled = false;
  bool isLoading = false;
  bool isLoggedIn = false;
  String token = '';

  @override
  void initState() {
    super.initState();
    _checkCoreAppInstalled();
  }

  Future<void> _checkCoreAppInstalled() async {
    try {
      if (await AppCheck.isAppEnabled('app.aksa.notes')) {
        log('App Installed');
        setState(() {
          isAppInstalled = true;
        });
      } else {
        log('App Not Installed');
      }
    } catch (err) {
      log('App Not Installed');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: Stack(
        children: [
          Center(
            child: Padding(
              padding: const EdgeInsets.all(24),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  if (isLoggedIn)
                    Container(
                      padding: const EdgeInsets.symmetric(
                        vertical: 4,
                        horizontal: 8,
                      ),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                        color: Colors.green.withOpacity(.3),
                      ),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Container(
                            height: 6,
                            width: 6,
                            decoration: const BoxDecoration(
                              color: Colors.green,
                              shape: BoxShape.circle,
                            ),
                          ),
                          const SizedBox(
                            width: 8,
                          ),
                          const Text(
                            'login status true',
                            style: TextStyle(
                              color: Colors.green,
                            ),
                          ),
                        ],
                      ),
                    ),
                  MaterialButton(
                    onPressed: () async {
                      setState(() {
                        isLoading = true;
                      });
                      log('\n-\n-\n-GET TOKEN\n-\n-\n-');
                      final savedToken = await ContentProviderHelper.getToken();

                      if (!savedToken.values.contains(null)) {
                        log('\n-\n-\n-TOKEN RETRIEVED \n Raw Token(token: ${savedToken['token']}, iv: ${savedToken['iv']})\n-\n-\n-');
                        decryptToken(savedToken);
                      } else {
                        final result = await KeycloakHelper.connect();
                        if (!result.containsValue(null) ||
                            !result.containsValue('')) {
                          await ContentProviderHelper.storeToken(
                            result['token'].toString(),
                            result['iv'].toString(),
                          );

                          decryptToken(result);
                        } else {
                          log('Cant login');
                        }
                      }
                    },
                    minWidth: double.maxFinite,
                    color: Colors.redAccent,
                    child: Text(
                      'Login SSO',
                      style: Theme.of(context).textTheme.labelMedium?.copyWith(
                            color: Colors.white,
                          ),
                    ),
                  ),
                  MaterialButton(
                    onPressed: () async {
                      // await KeycloakHelper.deleteAccount();
                      // await ContentProviderHelper.store("");

                      setState(() {
                        isLoggedIn = false;
                        token = "";
                      });
                    },
                    minWidth: double.maxFinite,
                    color: Colors.redAccent,
                    child: Text(
                      'Logout',
                      style: Theme.of(context).textTheme.labelMedium?.copyWith(
                            color: Colors.white,
                          ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          if (isLoading)
            Container(
              height: double.maxFinite,
              width: double.maxFinite,
              color: Colors.black26.withOpacity(.3),
              child: const Center(
                child: CircularProgressIndicator(),
              ),
            )
        ],
      ),
    );
  }

  void decryptToken(Map<Object?, Object?> rawToken) async {
    log('\n-\n-\n-DECRYPT TOKEN\n-\n-\n-');
    final _token = await ContentProviderHelper.decryptToken(
      rawToken['token'].toString(),
      rawToken['iv'].toString(),
    );

    setState(() {
      isLoggedIn = true;
      token = _token;
      isLoading = false;
    });
    log('\n-\n-\n-TOKEN DECRYPTED Token($_token)\n-\n-\n-');
  }
}
