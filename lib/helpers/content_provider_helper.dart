import 'dart:developer';

import 'package:flutter/services.dart';

class ContentProviderHelper {
  ContentProviderHelper._();

  static const platform = MethodChannel('CHAT_AI_NATIVE_HELPER');

  static Future<String> decryptToken(String token, String iv) async {
    try {
      var arguments = {'token': token, 'iv': iv};
      final result = await platform.invokeMethod("decrypt", arguments);
      return result as String;
    } catch (err) {
      throw PlatformException(
        code: 'PLATFORM_EXCEPTION',
        message: err.toString(),
      );
    }
  }

  static Future<void> storeToken(String token, String iv) async {
    try {
      var arguments = {'token': token, 'iv': iv};
      await platform.invokeMethod("storeToken", arguments);
    } catch (err) {
      print("Error delete token $err");
      throw PlatformException(
        code: 'PLATFORM_EXCEPTION',
        message: err.toString(),
      );
    }
  }

  static Future<Map<Object?, Object?>> getToken() async {
    try {
      final result = platform.invokeMethod('getToken');
      final result0 = await result;

      return result0 as Map<Object?, Object?>;
    } catch (err) {
      throw PlatformException(
        code: 'PLATFORM_EXCEPTION',
        message: err.toString(),
      );
    }
  }
}
