import 'dart:developer';
import 'package:flutter/services.dart';

class KeycloakHelper {
  const KeycloakHelper._();

  static const platform = MethodChannel('CHAT_AI_NATIVE_HELPER');

  static Future<bool?> init() async {
    try {
      final result = platform.invokeMethod('init');
      return result as bool?;
    } catch (err) {
      return false;
    }
  }

  static Future<Map<Object?, Object?>> connect() async {
    try {
      final result = platform.invokeMethod('connect');
      final result0 = await result;

      return result0 as Map<Object?, Object?>;
    } catch (err) {
      throw PlatformException(
        code: 'PLATFORM_EXCEPTION',
        message: err.toString(),
      );
    }
  }
}
